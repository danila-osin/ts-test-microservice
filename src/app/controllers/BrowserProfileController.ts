import { Controller, Get, Param } from 'routing-controllers';

import { container } from '../../inversify-config';
import { Types } from '../../inversify-config/types';
import { IBrowserProfileService } from '../services/interfaces/IBrowserProfileService';

@Controller('/v1.0/browser_profiles')
export class BrowserProfileController {
  private _browserProfileService: IBrowserProfileService;

  constructor() {
    this._browserProfileService = container.get(Types.BrowserProfileService);
  }
  
  @Get('/')
  async getAll() {
    return await this._browserProfileService.getAll();
  }

  @Get('/:id')
  async getOne(@Param('id') id: string) {
    return await this._browserProfileService.getById(id);
  }
}