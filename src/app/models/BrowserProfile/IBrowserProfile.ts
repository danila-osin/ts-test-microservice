import { Document } from 'mongoose';

import { PlatformEnum } from '../../../utils/enums/PlatformEnum';
import { ICompany } from '../Company/ICompany';
import { IUser } from '../User/IUser';

export interface IBrowserProfile extends Document {
  id: string;

  company: ICompany;
  user: IUser;

  name: string;

  platform: PlatformEnum;

  //TODO set strict type
  proxy: any;
  tags: string[];
}