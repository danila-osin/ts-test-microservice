import { Document } from 'mongoose';

import { LanguageEnum } from '../../../utils/enums/LanguageEnum';
import { ThemeEnum } from '../../../utils/enums/ThemeEnum';
import { ICompany } from '../Company/ICompany';

export interface IUser extends Document {
  company: ICompany;

  username: string;
  password: string;

  columns: string[];
  columnsWidth: any;
  columnsPresets: {
    name: string,
    columns: string[]
  }[];

  settings: {
    language?: LanguageEnum,
    theme: ThemeEnum,
    dolphinIntegration: {
      token?: string
    }
  }

  comparePassword(password: string, next: (same: any, err: any) => any): any;
}