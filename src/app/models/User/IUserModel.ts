import { Model } from 'mongoose';

import { IUser } from './IUser';

export interface IUserModel extends Model<IUser> {}