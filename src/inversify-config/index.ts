import { Container } from 'inversify';

import { prodContainer } from './config';
import { testContainer } from './config-test';

let container: Container;

if (process.env.NODE_ENV === 'test') {
  container = testContainer;
} else {
  container = prodContainer;
}

export { container };