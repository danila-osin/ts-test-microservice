import 'reflect-metadata';

import { useContainer } from 'class-validator';
import express from 'express';
import { useExpressServer } from 'routing-controllers';

import { BrowserProfileController } from './app/controllers/BrowserProfileController';
import { container } from './inversify-config';

const main = async () => {
  const app = express();

  useContainer(container);
  useExpressServer(app, {
    routePrefix: '/api',
    controllers: [
      BrowserProfileController
    ]
  });

  app.listen(5000, () => console.log('Server started...'));
}

main();